#
# Be sure to run `pod lib lint FederatedLoginAt.podspec' to ensure this is a
# valid spec before submitting.
#
Pod::Spec.new do |s|
  s.name             = "FederatedLoginAt"
  s.version          = "4.0.0"
  s.summary          = "Federated login library for Austria"
  s.description      = "Federated login library for Austria, get tokens"
  s.homepage         = "https://bitbucket.org/alperkal/federatedloginat.git"
  s.license          = 'Private'
  s.author           = { "Alper Sabri Kalaycioglu" => "alperkal@gmail.com" }
  s.source           = { :git => "https://bitbucket.org/alperkal/federatedloginat.git", :tag => s.version.to_s }

  s.platform     = :ios, '11.0'

  s.requires_arc = true
  s.source_files = 'Pod/Classes/**/*'

  s.resource_bundles = {
    'EncryptionJs' => ['Pod/Js/rsa.js','Pod/Js/jsbn.js','Pod/Js/rng.js','Pod/Js/prng4.js']
  }

  s.dependency 'Just', '~> 0.7.1'
  s.dependency 'Fuzi', '~> 2.0.2'
  s.dependency 'CryptoSwift', '~> 0.13.0'
  
  s.pod_target_xcconfig = { 'HEADER_SEARCH_PATHS' => "$(SDKROOT)/usr/include/libxml2" }

end
