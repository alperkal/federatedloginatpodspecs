#
# Be sure to run `pod lib lint FederatedLoginAt.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "FederatedLoginAt"
  s.version          = "0.1.1"
  s.summary          = "Federated login library for Austria"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = 	"Federated login library for Austria, get tokens"
                       

  s.homepage         = "https://bitbucket.org/alperkal/federatedloginat.git"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Alper Sabri Kalaycioglu" => "alperkal@gmail.com" }
  s.source           = { :git => "https://alperkal@bitbucket.org/alperkal/federatedloginat.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'FederatedLoginAt' => ['Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'Just'
  s.dependency 'EZSwiftExtensions'
  s.dependency 'CryptoSwift'
end
